declare module "download-file" {
	interface Options {
		/**
		 * String with path to directory where to save files (default: current working directory)
		 */
		directory?: string;

		/**
		 * String for the name of the file to be saved as (default: filename in the url)
		 */
		filename?: string;

		/**
		 * Integer of how long in ms to wait while downloading (default: 20000)
		 */
		timeout?: number;
	}

	type Callback = (err: unknown) => void;

	/**
	 *
	 * @param url String of the file URL to download
	 * @param callback Function to run after
	 */
	function download(url: string, callback: Callback): void;

	/**
	 * @param url String of the file URL to download
	 * @param options Object with options
	 * @param callback Function to run after
	 */
	function download(url: string, options: Options, callback: Callback): void;

	export = download;
}
