# wiki-image-grabber

A simple Node script to download images from a Wiki using the MediaWiki API.

## Usage

1. Install dependencies with `npm ci` or `npm install`.
2. Edit `download.ts` and provide a `server` and `path` values.
   - For example, a `server` value of `"rain-web-comic.fandom.com/"` and `path` of `"api.php"` will grab all images from the old Fandom wiki for the [_Rain_](https://rain.thecomicseries.com/comics/first/#content-start) webcomic. (Great read, btw!)
3. Run the script with `npm start`.

Images will begin to download to the `images/` directory in the repository.

## Acknowledgements

This script is adapted from the one at https://dev.fandom.com/wiki/DownloadImages. Thanks to the author(s) there for their work.
