/**
 * Adapted from https://dev.fandom.com/wiki/DownloadImages
 */

import Bot = require("nodemw");
import download = require("download-file");

// Pass configuration object
const client = new Bot({
	protocol: "https", // Wikipedia now enforces HTTPS
	server: "rain-web-comic.fandom.com/", // Host name of MediaWiki-powered site
	path: "api.php", // Path to api.php script
	debug: false, // Is more verbose when set to true
});

interface Page {
	title: string;
}

interface ImageInfo {
	descriptionurl: string;
	url: string;
}

client.getPagesInNamespace(6, function (err, data: ReadonlyArray<Page>) {
	// Error handling
	if (err) {
		console.error(err);
		return;
	}
	for (const p of data) {
		client.getImageInfo(p.title, function (e, d: ImageInfo | null | undefined) {
			if (e) {
				console.error(e);
				return;
			}
			if (!d) {
				console.log(p.title);
				return;
			}
			const options = {
				directory: "./images/",
				filename: d.descriptionurl.replace(/^http.*?\/File:/, ""),
			};
			download(d.url, options, function (err) {
				if (err) throw err;
			});
		});
	}
});
